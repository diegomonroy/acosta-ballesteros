<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 medium-4 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-8 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->