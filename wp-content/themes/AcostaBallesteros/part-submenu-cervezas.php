<!-- Begin Submenu Cervezas -->
	<section class="submenu" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php
				wp_nav_menu(
					array(
						'menu_class' => 'menu align-center',
						'container' => false,
						'theme_location' => 'cervezas-menu',
						'items_wrap' => '<ul class="%2$s" data-dropdown-menu>%3$s</ul>'
					)
				);
				?>
			</div>
		</div>
	</section>
<!-- End Submenu Cervezas -->