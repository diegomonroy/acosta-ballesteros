<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'data' ); ?>
			</div>
			<div class="small-12 medium-6 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'social_media' ); ?>
			</div>
		</div>
	</section>
	<section class="top_1" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 medium-9 columns">
				<?php dynamic_sidebar( 'menu' ); ?>
			</div>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'search' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->