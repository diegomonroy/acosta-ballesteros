<?php
if ( is_product_category( array( 'cervezas', 'alemanas', 'belgica', 'chile', 'china', 'colombia', 'espana', 'holanda', 'inglaterra', 'italia', 'mexico', 'rusia', 'usa' ) ) ) : ?>
<?php get_template_part( 'part', 'submenu-cervezas' ); ?>
<?php endif; ?>
<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->