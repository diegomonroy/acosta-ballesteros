<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'empresa' ) ) ) : dynamic_sidebar( 'banner_nuestra_empresa' ); endif; ?>
				<?php if ( is_page( array( 'cervezas' ) ) ) : dynamic_sidebar( 'banner_cervezas' ); endif; ?>
				<?php if ( is_page( array( 'barriles' ) ) ) : dynamic_sidebar( 'banner_barriles' ); endif; ?>
				<?php if ( is_page( array( 'licores' ) ) ) : dynamic_sidebar( 'banner_licores' ); endif; ?>
				<?php if ( is_page( array( 'energizantes' ) ) ) : dynamic_sidebar( 'banner_energizantes' ); endif; ?>
				<?php if ( is_page( array( 'vinos' ) ) ) : dynamic_sidebar( 'banner_vinos' ); endif; ?>
				<?php if ( is_page( array( 'accesorios' ) ) ) : dynamic_sidebar( 'banner_accesorios' ); endif; ?>
				<?php if ( is_page( array( 'estuches' ) ) ) : dynamic_sidebar( 'banner_estuches' ); endif; ?>
				<?php if ( is_page( array( 'ubicacion' ) ) ) : dynamic_sidebar( 'banner_ubicacion' ); endif; ?>
				<?php if ( is_page( array( 'contactenos' ) ) ) : dynamic_sidebar( 'banner_contactenos' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->